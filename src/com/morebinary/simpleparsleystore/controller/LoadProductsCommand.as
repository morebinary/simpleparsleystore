/** @author: Paras Sheth <paras@morebinary.com> */
package com.morebinary.simpleparsleystore.controller
{
	import com.morebinary.simpleparsleystore.messages.LoadProductsMessage;
	import com.morebinary.simpleparsleystore.model.Product;
	import com.morebinary.simpleparsleystore.model.ProductsCollection;
	import com.morebinary.simpleparsleystore.services.ProductsService;
	
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class LoadProductsCommand
	{
		[Inject]
		public var service:ProductsService;
		
		[Inject]
		public var product:Product;
		
		[Inject]
		public var productsCollection:ProductsCollection;
		
		private var _productIndex:int;
		
		
		/**
		 * Execute this command
		 */
		public function execute(message:LoadProductsMessage):void
		{
			service.send().addResponder( new Responder(result, error) );
		}
		
		/**
		 * The xml is retrieved and the array collection is populated
		 */
		private function result(event:ResultEvent):void
		{
			var productList:XMLList = (event.result.product as XMLList);
			
			for each(var product:XML in productList)
			{
				productsCollection.items.addItem( getProduct(product) );
				_productIndex++;
			}
		}
		
		/**
		 * Parses the xml into Product value objects
		 */
		private function getProduct(product:XML):Product
		{
			var productVO:Product = new Product();
			productVO.index = _productIndex;
			productVO.name = product.name;
			productVO.price = product.price;
			productVO.picture = product.picture;
			productVO.description = product.description;
			
			return productVO;
		}
		
		/**
		 * Handles the xml error
		 */
		private function error(event:FaultEvent):void
		{
			trace("Error: " + event.fault);
		}

	}
}