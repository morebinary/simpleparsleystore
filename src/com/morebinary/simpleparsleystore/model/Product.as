/** @author: Paras Sheth <paras@morebinary.com> */
package com.morebinary.simpleparsleystore.model
{
	[Bindable]
	public class Product
	{
		/**
		 * a unique index for the product 
		 */
		public function get index():int{return _index;}
		public function set index(value:int):void
		{
			_index = value;
		}
		private var _index:int;
		
		/**
		 * name for the product 
		 */
		public function get name():String{return _name;}
		public function set name(value:String):void
		{
			_name = value;
		}
		private var _name:String;
		
		/**
		 * image url for the product 
		 */
		public function get picture():String{return _picture;}
		public function set picture(value:String):void
		{
			_picture = value;
		}
		private var _picture:String;

		/**
		 * description for the product 
		 */
		public function get description():String{return _description;}
		public function set description(value:String):void
		{
			_description = value;
		}
		private var _description:String;

		/**
		 * price of the product 
		 */
		public function get price():Number{return _price;}
		public function set price(value:Number):void
		{
			_price = value;
		}
		private var _price:Number;

		
		/**
		 * flag indicating whether or not the product has been added to the cart
		 */
		public function get inBasket():Boolean{return _inBasket;}
		public function set inBasket(value:Boolean):void
		{
			_inBasket = value;
		}
		private var _inBasket:Boolean;
		
		/**
		 * This method exist for unit testing purposes only
		 */
		public function toParamObject():Object
		{
			var params:Object = new Object();
			params["index"] = index >= 0 ? index : null;
			params["name"] = name;
			params["description"] = description;
			params["price"] = price;
			
			return params;
		}
	}
}