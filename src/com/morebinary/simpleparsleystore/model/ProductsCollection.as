/** @author: Paras Sheth <paras@morebinary.com> */
package com.morebinary.simpleparsleystore.model
{
	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	
	[Bindable]
	public class ProductsCollection
	{
		/**
		 * An ArrayCollection of Product value objects
		 */
		public function get items():ArrayCollection{return _items;}
		public function set items(value:ArrayCollection):void
		{
			_items = value;
		}
		private var _items:ArrayCollection = new ArrayCollection();

		/**
		 * Holds currently selected item index
		 */
		public function get selectedIndex():int{return _selectedIndex;}
		public function set selectedIndex(value:int):void
		{
			_selectedIndex = value;
		}
		private var _selectedIndex:int;

		/**
		 * Total amount for the purchase
		 */
		public function get totalPurchase():Number{return _totalPurchase;}
		public function set totalPurchase(value:Number):void
		{
			_totalPurchase = value;
		}
		private var _totalPurchase:Number = 0;

	}
}