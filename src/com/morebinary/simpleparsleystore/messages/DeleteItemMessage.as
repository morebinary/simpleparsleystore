/** @author: Paras Sheth <paras@morebinary.com> */
package com.morebinary.simpleparsleystore.messages
{
	
	public class DeleteItemMessage
	{
		public function get itemIndex():int{return _itemIndex;}
		public function set itemIndex(value:int):void
		{
			_itemIndex = value;
		}
		private var _itemIndex:int;
	}
}