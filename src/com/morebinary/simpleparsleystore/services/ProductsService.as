/** @author: Paras Sheth <paras@morebinary.com> */
package com.morebinary.simpleparsleystore.services
{
	import com.morebinary.simpleparsleystore.model.Product;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.http.HTTPService;
	
	public class ProductsService
	{
		[Inject]
		public var product:Product;
		
		[Inject]
		public var service:HTTPService;
		
		/**
		 * Main wrapper token to respond to all registered responders
		 */
		public var token:AsyncToken = new AsyncToken();
		
		/**
		 * loads the xml
		 */
		public function send():AsyncToken
		{
			var localToken:AsyncToken;
			localToken = service.send();
			return localToken;
		}
	}
}