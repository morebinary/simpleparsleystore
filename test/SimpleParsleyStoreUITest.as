/** @author: Paras Sheth <paras@morebinary.com> */
package
{
	import com.morebinary.simpleparsleystore.view.ProductDetailsPanel;
	import com.morebinary.simpleparsleystore.view.ProductsList;
	
	import mx.events.FlexEvent;
	
	import org.flexunit.assertThat;
	import org.flexunit.async.Async;
	import org.fluint.uiImpersonation.UIImpersonator;
	import org.hamcrest.object.notNullValue;

	public class SimpleParsleyStoreUITest
	{		
		private var _productsList:ProductsList;
		private var _productDetailsPanel:ProductDetailsPanel;
		
		[Before(async, ui, order="1")]
		public function setUpList():void
		{
			_productsList = new ProductsList();
			Async.proceedOnEvent(this, _productsList, FlexEvent.CREATION_COMPLETE);
			UIImpersonator.addChild(_productsList);
		}

		[Before(async, ui, order="2")]
		public function setUpPanel():void
		{
			_productDetailsPanel = new ProductDetailsPanel();
			Async.proceedOnEvent(this, _productDetailsPanel, FlexEvent.CREATION_COMPLETE);
			UIImpersonator.addChild(_productDetailsPanel);
		}
		
		[Test]
		public function checkTheComponents():void
		{
			assertThat(_productsList, notNullValue());
			assertThat(_productDetailsPanel, notNullValue());
		}
		
		[After(async, ui)]
		public function tearDown():void
		{
			UIImpersonator.removeChild(_productsList);
			_productsList = null;

			UIImpersonator.removeChild(_productDetailsPanel);
			_productDetailsPanel = null;
		}
	}
}