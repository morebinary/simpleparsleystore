/** @author: Paras Sheth <paras@morebinary.com> */
package com.morebinary.simpleparsleystore.helper
{
	import mx.collections.XMLListCollection;
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.http.HTTPService;
	
	import org.flexunit.runner.external.ExternalDependencyToken;
	import org.flexunit.runner.external.IExternalDependencyLoader;
	
	
	public class XMLDataHelper implements IExternalDependencyLoader, IResponder
	{
		private var edToken:ExternalDependencyToken;
		private var service:HTTPService;
		
		/**
		 * Creates a new XMLDataHelper
		 *  
		 * @param url external data(dependency) url
		 */
		public function XMLDataHelper(url:String)
		{
			service = new HTTPService();
			service.url = url;
			service.resultFormat = "e4x";
			
			edToken = new ExternalDependencyToken();
		}
		
		/**
		 * The only required method implementation for IExternalDependencyLoader
		 * 
		 * @param testClass Class to find and resolve dependencies on
		 */
		public function retrieveDependency(testClass:Class):ExternalDependencyToken
		{
			var asyncToken:AsyncToken = service.send();
			asyncToken.addResponder( this );
			
			return edToken;
		}
		
		/**
		 * Result handler
		 */
		public function result(data:Object):void
		{
			var array:Array = new Array();
			
			var list:XMLList = data.result..product;
			
			//Adding each XML into the Array
			for(var i:int=0; i < list.length(); i++)
			{
				array.push(list[i]);
			}
			
			edToken.notifyResult( array );
		}
		
		
		/**
		 *Error handler
		 */
		public function fault(info:Object):void
		{
			edToken.notifyFault("Could not load external data");
		}
	}
}