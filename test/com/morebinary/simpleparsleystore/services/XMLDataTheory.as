/** @author: Paras Sheth <paras@morebinary.com> */
package com.morebinary.simpleparsleystore.services
{
	import com.morebinary.simpleparsleystore.helper.XMLDataHelper;
	
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	import mx.collections.XMLListCollection;
	
	import org.flexunit.assertThat;
	import org.flexunit.asserts.assertTrue;
	import org.flexunit.assumeThat;
	import org.flexunit.runner.external.IExternalDependencyLoader;
	import org.fluint.sequence.SequenceCaller;
	import org.fluint.sequence.SequenceRunner;
	import org.fluint.sequence.SequenceWaiter;
	import org.hamcrest.core.isA;
	import org.hamcrest.number.greaterThan;
	import org.hamcrest.number.isNotANumber;

	[RunWith("org.flexunit.experimental.theories.Theories")]
	public class XMLDataTheory
	{
		public static var xmlLoader:IExternalDependencyLoader = new XMLDataHelper("assets/xml/products.xml");
		
		[DataPoints(loader="xmlLoader")]
		[ArrayElementType("XML")]
		public static var productsXMLList:Array;
		
		[Theory]
		public function shouldAllPricesBeAPositiveNumber(product:XML):void
		{
			assumeThat(product, isA(XML));
			assertThat(Number(product.price), greaterThan(0));
		}

		[Theory]
		public function shouldAllProductNamesBeNotEmpty(product:XML):void
		{
			assumeThat(product, isA(XML));
			assertTrue(product.name != "");
		}

		[Theory]
		public function shouldAllProductDescBeNotEmpty(product:XML):void
		{
			assumeThat(product, isA(XML));
			assertTrue(product.description != "");
		}

		[Theory(async)]
		public function shouldAllProductImagesURLBeNotEmptyAndValid(product:XML):void
		{
			assumeThat(product, isA(XML));
			
			assertTrue(product.picture != "");
			
			var loader:Loader = new Loader();
			
			var sequence:SequenceRunner = new SequenceRunner( this );
			
			sequence.addStep( new SequenceCaller(loader, loader.load, [new URLRequest(product.picture)]) );
			sequence.addStep( new SequenceWaiter(loader.contentLoaderInfo, Event.COMPLETE, 100) );
				
			sequence.addAssertHandler( handleSequenceComplete, loader );
				
			sequence.run();
			
		}
		
		private function handleSequenceComplete(event:Event, passThroughData:Object):void
		{
			trace("Image was successfully loaded : Total bytes = " + passThroughData.contentLoaderInfo.bytesTotal);
		}
		
	}
}