/** @author: Paras Sheth <paras@morebinary.com> */
package com.morebinary.simpleparsleystore.model
{
	import flexunit.framework.Assert;
	
	import org.flexunit.asserts.assertFalse;
	import org.hamcrest.assertThat;
	import org.hamcrest.object.hasProperties;
	
	public class ProductTest
	{		
		[Test(description="This is a generic object test with correct values supplied")]
		public function testToParamObject_returnsGenericObjectWithCorrectPropertyValues():void
		{
			var paramObject:Object;
			var product:Product = new Product();
			product.index = 1;
			product.name = "Some product name";
			product.description = "Some description";
			product.price = 44;
			
			paramObject = product.toParamObject();
			
			assertThat( product, hasProperties(paramObject) );
		}
		
		[Test(description="This tests whether or not the default value for inBasket proprty is false")]
		public function testToParamObject_checkDefaultInBasketValue():void
		{
			var paramObject:Object;
			var product:Product = new Product();
			
			paramObject = product.toParamObject();
			
			assertFalse(paramObject["inBasket"]);
		}
	}
}